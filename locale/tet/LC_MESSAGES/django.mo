��    P      �  k         �     �  	   �  	   �     �     �               +     4     C     H     M     U     [  	   i     s     |     �     �     �     �     �     �     �  �   �     �     �     �     �  
   �     �     �     �     	     $	     )	     .	  (   =	     f	  
   z	  ;   �	     �	     �	     �	  	   �	     �	     
     
     
  	   +
     5
  
   B
     M
     [
     r
     �
     �
     �
     �
  
   �
  	   �
     �
     �
  
   �
  =   �
  �  3  L   �               3     H     X     f     �  L   �     �     �          &  e  +     �  
   �  
   �     �  
   �     �     �     �               #     )     0     6     E     U     a     m     t     �     �  #   �     �     �  �   �     �     �     �     �     �     �     �       +   
     6     <     E  &   X       
   �  ;   �     �     �                     =     F     S     a     i  	   x     �     �     �     �     �     �     �     �            
        (  =   .  �  l  Y   C     �     �     �     �     �  $   �  %     R   E     �     �     �     �     D       E   7          ;   '           0   ?       L   F   A   :           $   ,      J         2              P   6      #                 !       5   )          .   H   1               <      4   *             &   %          9                            M      -                        8   /   G      	      N       (   3   I   O      K   =   
      @       C   "   +   >             B          Address Address 1 Address 2 Become A Member Become a Member Body of Inquiry Book Exhibition Space Book Now Book Our Space City Cost Country Email Email Address Embed URL End date End time English Enter the message Enter your email Enter your name Enter your phone External link Home In our interactive space, we would like to have a book club, conduct user surveys and promote discussion around a vast array of topics. Sign up as a member to find out more. Interactive Space Keep Reading Latest Events Learning Resources Link title Make an Inquiry Make an inquiry Message Monday–Saturday: 9am – 5pm Name Next No blogs found Not required if event is on a single day Ongoing Exhibitions Open Hours Optional - form submissions will be emailed to this address Organisation Our News Phone Number Post Code Prefered Form of Contact Previous Reading Room Related links Resources Send Message Start time Stay in Touch Submit Booking Inquiry Submit Membership Submit inquiry Sunday: Closed Support the Reading Room Surname Talk to Us Telephone Tetun The Library The Museum The Reading Room is closed on public holidays in Timor-Leste. The Xanana Gusmão Reading Room is a free for the public library, museum and community space in Dili, Timor-Leste. The Reading Room was opened in June of 2000 by founder Kirsty Sword-Gusmão with the goal of being a safe, open space for the community to learn and thrive- a culture that continues to be upheld today in everything that we do. Sign up to our newsletter to keep in touch with what we are doing. The Xanana Gusmão Reading Room is closed on public holidays in Timor-Leste. Title Tourist Information Upcoming Exhibitions View All Events View All News View All Permanent Exhibitions View All Temporary Exhibitions We would love to discuss having your exhibition in our space. Inquire today! What is on? beside Kmanek Supermarket carousel_items name Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-12-28 14:13+0000
PO-Revision-Date: 2018-01-26 11:34+0000
Last-Translator: <mossplix@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Translated-Using: django-rosetta 0.7.13
 Enderesu Enderesu 1 Enderesu 2  Rejistu Sai Membru Sai Membru Konteudu Pedidu Espasu exposisaun Livru Rezerva agora Reserva Ami-nia espasu Sidade Folin Nasaun Email Enderesu Email URL Incomporado Data Ramata Oras Ramata Ingles Hakerek Mensajem Hatama ita-boot nia email Hatama Ita-boot nia Naran Hatama ita-boot nia numeru telefone Link Eksternal Inisiu Iha ami nia espasu interativu, ami hakarak atu iha klubu livru, halao survei user no promove diskusaun ho topiku oin-oin. Rejistu sai membru atu hetan liu tan.  Espasu Interativu Konitnua Lee Eventu Ikus Nian Rekursu ba Buka Matenek Titulu Link Halo Peskiza Halo Pedidu Mensajen  Segunda-Sabadu: 09 dader to'o 05 lorokraik Naran Tuir mai La hetan blog ruma La persija se eventu ne'e ba loron ida Espozisaun ne'ebe halao hela Oras Hahuu Opsional-formulariu submisaun sei email ba endersu ida ne'e Organizasaun Ami-nia Notisia Numeru Telefone Kodigu Postal  Perefensia Forma ba Kontaktu  Prevista Sala Leitura Link Ligasaun Rekursu Manda Mensajen Oras Hahu Manten Kontaktu Hatama Pedidu ba Rezerva Fatin Hatama Rejistu Membru Hatama Pedidu Domingo : Taka Apoiu Sala Leitura Apelidu Koalia mai ami Telefone Tetun Bibleoteka Muzeu Sala Leitura taka iha loron feriadu publiku iha Timor Leste.  Sala Leitura Xanana Gusmao hanesan fatin livre ba biblioteka publiku, museum no hanesan espasu komunidade nian iha Dili, Timor-Leste. Sala Leitura hahuu loke iha fulan Junho 2000 husi fundador Sra. Kirsty Sword Gusmao ho objetivu sai hanesan fatin seguru, espasu livre ba komunidade atu aprende no dezenvolve kultura ne'ebe mak kontinua mantem to . . ohin loron  ho buat hotu ne'ebe mak ita halo. Rejistu mai ami nia newsletter atu manten kontaktu ho saida mak ami halo. Sala de Leitura Xanana Gusmao sei taka wainhira  iha 
 loron feriadu publiku Timor Leste. Titulu Informasaun Turista Espozisaun Tuir mai Hare Eventu Hotu-hotu  Hare Notisia Hotu-hotu Hare Espozisaun Permanente hotu-hotu  Hare Espozisaun temporariu hotu-hotu  Ami hakarak halo diskusaun ho ita nia exposisaun iha mai nia Espasu. Husu agora!  Saida mak Akontese?  Kmanek Supermarket nia sorin Item Carousel naran 